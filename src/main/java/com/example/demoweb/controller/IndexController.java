/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demoweb.controller;

import java.util.Collections;
import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Jaime Cabrera
 */
@Controller
@RequestMapping("/")
public class IndexController {
    
    @GetMapping
    public Map<String,String> index(){
        return Collections.singletonMap("mensaje", "index");
    }
}
